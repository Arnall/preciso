
## This makefile helps delivering new versions of PreciSo on PyPi
##
## Steps :
##   1. Change version number in setup.py
##   2. Add changes description to CHANGELOG.md
##   3. Create a new git tag (from https://gitlab.com/Arnall/preciso/-/tags) named after the version (eg 1.0.x). It helps knowing which commit corresponds to the PyPi package version.
##   4. Have an account and an authentification token for PreciSo on PyPi.org. Needs the proper access rights to modify preciso.
##   5. Do 'make distrib' to create the package locally under the dist/ folder. Review it (extract the archive if necessary) to hunt for bugs. PyPi versions are not modifiable afterhand !
##   6. Do 'make upload_test' to upload to testPyPi (review, download...) 
##	Note: It can be installed in Jupiter via:
##	!pip install -i https://test.pypi.org/simple/ --upgrade Preciso
##   7. Do 'make upload' to upload to PyPi. 
##      Note: upload will ask for a username/password. Use __token__ as username, and paste the authentification token as password.
##      Note: It can be installed in Jupiter via:
##	!pip install --upgrade Preciso


all: distrib

distrib: clean update
	python3 setup.py sdist bdist_wheel

upload:
	python3 -m twine upload dist/* --skip-existing

clean:
	rm -rf build dist *.egg-info

update:
	python3 -m pip install --upgrade setuptools wheel twine

upload_test:
	python3 -m twine upload --repository testpypi dist/* --skip-existing